# create_db.py -- create a spanis dictionary sql database
# Copyright (C) 2019 Jordan Russell <jordan.likes.curry@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sqlite3
import sys
import xml.etree.ElementTree as ET

def initialize_db(filename="spanish_dict.db"):
    conn = sqlite3.connect(filename)
    c = conn.cursor()
    try:
        c.execute('''CREATE TABLE dict (
        spanish TEXT,
        english TEXT,
        class TEXT);''')
        c.execute('''CREATE INDEX engidx ON dict(english);''')
        c.execute('''CREATE INDEX spidx ON dict(spanish);''')
        c.execute('''CREATE VIRTUAL TABLE englishfts USING fts5(english);''')
        c.execute('''CREATE VIRTUAL TABLE spanishfts USING fts5(spanish);''')
        conn.commit()
    except sqlite3.OperationalError as e:
        print(e)
        sys.exit(1)

    return conn


def build_dict(conn,
               filename="./en-es-en-Dic/src/main/resources/dic/es-en.xml"):
    tree = ET.parse(filename)
    root = tree.getroot()

    cursor = conn.cursor()


    for l_node in root:
        for w in l_node:
            spanish = w[0].text
            english = w[1].text
            lexclass = w[2].text
            try:
                cursor.execute("INSERT INTO dict VALUES (?, ?, ?)",
                               (spanish,english,lexclass))
                cursor.execute("INSERT INTO englishfts VALUES (?)",
                               (english,))
                cursor.execute("INSERT INTO spanishfts VALUES (?)",
                                (spanish,))
            except sqlite3.OperationalError as e:
                print(e)

    conn.commit()

def main():
    conn = initialize_db()
    dir = "./en-es-en-Dic/src/main/resources/dic"
    nouns = dir + "/es-en.xml"
    verbs = dir + "/verbs/es-en.xml"
    build_dict(conn, nouns)
    build_dict(conn, verbs)
    conn.close()

if __name__ == '__main__':
    main()

