# db_interface.py -- lookup words in the dictionary database
# Copyright (C) 2019 Jordan Russell <jordan.likes.curry@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sqlite3
import re

def lookup_spanish(term,c):
    result = c.execute('''
    SELECT spanish, english, class FROM dict
    WHERE spanish = ?;
    ''', (term,))

    return result.fetchall()

def lookup_english(term,c):
    result = c.execute('''
    SELECT spanish, english, class FROM dict
    WHERE english in (SELECT english from englishfts where englishfts MATCH ?);
    ''', (term,))
    return result.fetchall()

def lookup(term, cursor):
    """Guess whether the word is spanish or English
    or not and look it up in the appropriate dictionary.
    term: A word to look up.
    cursor: a sqlite3 database cursor"""
    if is_spanish_word(term):
        spanish = lookup_spanish(term, cursor)
        if spanish:
            return spanish
    return lookup_english(term, cursor)

def is_spanish_word(term):
    """Detect if a word is Spanish and not English.
    Assuming that there are only Spanish and English words"""
    accented_vowels = set(["á", "é", "í", "ó", "ú", "ý"])
    vowels = set(["a", "e", "i", "o", "u", "y"]).union(accented_vowels)
    consonants = set(["b","v","c", "ch", "d", "f", "g", "gu",
                     "gü", "h", "hi", "hu", "j", "k", "l", "ll", "m",
                     "n", "ñ", "p", "qu", "r", "rr", "s", "sh", "t",
                     "tl", "tx", "tz", "w", "x", "y", "z"])
    semivowels = set(["j", "w"])

    for char in term:
        if char in (accented_vowels.union(set(["ñ"]))):
            return True

    ## Phonotactics time!
    ## A spanish word follows the pattern:
    #######################################
    ## (C1 (C2)) (S1) Vowel (S2) (C3 (C4))
    #######################################
    ## Where C1 is any consonant
    ## C2 iff C1 is /p t k b d g/ or /f/ OR C2 is /l r/ with not restrictions
    ## S1 is an optional semivowel
    ## Vowel is a vowel
    ## S2 is an optional semivowel
    ## The Coda C3 can be any consonant
    ## C4 must be /s/

    ## NOTE: orthography only tends to approximate spanish allophones,
    ## and here we are using orthography,
    ## but that it good enough for now

    ## first we break everything into tokens
    i = 0
    tokens = []
    everyone = vowels.union(consonants).union(semivowels)
    while i < len(term):
        if i+1 < len(term):
            if term[i:i+2] in everyone:
                tokens.append(term[i:i+2])
                i = i + 2
                continue
        if term[i] in everyone:
            tokens.append(term[i])
            i = i + 1
        else:
            return False


    def parse_syllable(pos):
        def parse_onset():
            nonlocal pos
            if tokens[pos] not in consonants:
                # There is no onset and we can move on
                return True

            pos = pos + 1
            if pos >= len(tokens):
                return False

            ## Next we parse optinal C2
            if tokens[pos] in consonants:
                if tokens[pos] in ["l", "r"]:
                    pos = pos + 1
                    if pos >= len(tokens):
                        return False
                elif tokens[pos-1] not in set(['p', 't', 'k', 'b', 'd', 'g', 'f']) or\
                    tokens[pos] not in ["l", "r"]:
                    return False
            else: # there was no consonant
                return True

        def parse_nucleus():
            nonlocal pos
            # (S1)
            if tokens[pos] in semivowels:
                pos = pos + 1
                if pos >= len(tokens):
                    return False
            # Vowel+ for dipthongs
            if tokens[pos] in vowels:
                while pos < len(tokens) and tokens[pos] in vowels:
                    pos = pos + 1
                if pos >= len(tokens):
                    return True # We are done now everything else is optional
            else:
                return False
            # (S2)
            if tokens[pos] in semivowels:
                pos = pos + 1
            return True


        def parse_coda():
            nonlocal pos
            if pos >= len(tokens):
                return True
            if tokens[pos] in consonants:
                pos = pos + 1
                if pos >= len(tokens):
                    return True

            # We are deliberately ignoring the rest of the input.
            # There's likely a spanish word in there anyway
            if tokens[pos] == "s":
                pos = pos + 1
            return True



        b = parse_onset() and  parse_nucleus() and parse_coda()
        return (pos, b)

    pos = 0
    while pos < len(tokens):
        pos, b = parse_syllable(pos)
        if not b:
            return False
    return True

