# website.py -- host a simple spanish dictionary
# Copyright (C) 2019 Jordan Russell <jordan.likes.curry@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import flask
import sqlite3
from flask import render_template
from flask import request, url_for, redirect
from flask import g
from db_interface import lookup

app = flask.Flask(__name__)

DATABASE = "./spanish_dict.db"

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/search/", methods=["GET", "POST"])
def empty_serach_url():
    if request.method == "GET":
        return redirect(url_for('index'))
    else:
        return redirect(url_for("results", terms=request.form['search']))

@app.route("/search/<terms>/", methods=["GET", "POST"])
def results(terms):
    cursor = get_db().cursor()
    print(terms.split(' '))
    results = [lookup(t,cursor) for t in terms.split(' ')]
    return render_template("results.html", results=results)


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()
